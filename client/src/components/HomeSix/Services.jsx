import React from 'react';
import { Link } from 'react-router-dom';
import ironman from '../../assets/photos/avatar.png'

import db from '../../db.json'
 
class Services extends React.Component {
    render(){
        return (
            <div style={{background: 'black'}}>
            <div style={{
                    backgroundImage: `url(${ironman})`, 
                    backgroundPosition: 'left', 
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover'
                }}>
            <section id="services" className="services-area uk-dark uk-services uk-section" style={{background: 'transparent'}}>
                <div className="uk-container">
                    <div className="uk-section-title section-title">
                        {/* <span>What I Do</span> */}
                        <h2>Skills</h2>
                        <div className="bar"></div>
                    </div>

                    <div className="uk-grid uk-grid-match uk-grid-medium uk-child-width-1-3@m uk-child-width-1-2@s">
                        
                        {db.skills.map(s =>
                            <div className="item">
                                <div className="single-services">
                                    <div className="icon">
                                        <i className="flaticon-plan"></i>
                                    </div>
                                    <h3>{s.name}</h3>
                                    <small>{s.details}</small>

                                    {/* <i className={`${s.icon} link-btn`}></i> */}

                                    {/* <Link to="#" className="link uk-position-cover"></Link> */}
                                </div>
                            </div>
                        )}

                    </div>
                </div>
            </section>
            </div>
            </div>
        );
    }
}
 
export default Services;