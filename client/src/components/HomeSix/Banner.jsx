import React from 'react';
import { Link } from 'react-router-dom';
import ModalVideo from 'react-modal-video';
import '../../../node_modules/react-modal-video/scss/modal-video.scss';

import bannerImg from '../../assets/photos/profile.jpg'
import db from '../../db.json' 

class Banner extends React.Component {
    state = {
        isOpen: false
    }

    openModal = () => {
        this.setState({isOpen: true})
    }
    render(){
        return (
            <div id="home" className="uk-banner uk-dark main-banner" style={{background: 'transparent'/*backgroundImage: `url(${bannerImg})`*/}}>
                <div className="d-table" style={{height: '80%'}}>
                    <div className="d-table-cell">
                        <div className="uk-container">
                            <img src={bannerImg} style={{position: 'absolute', right: '10%', width: '22%'}} />
                            <div className="main-banner-content">
                                <h1>Marco <br /> Cavallo</h1>
                                <p>{db.information.description}</p>
                                <a href="/resume.pdf" target="_blank" className="uk-button uk-button-default">Download resume</a>
                                {/*
                                <Link 
                                    onClick={e => {e.preventDefault(); this.openModal()}}
                                    to="#" 
                                    className="video-btn popup-youtube">
                                        <span uk-icon="play"></span> Watch Demo Reel
                                </Link>
                                */}

                                <ModalVideo 
                                    channel='youtube' 
                                    isOpen={this.state.isOpen} 
                                    videoId='bk7McNUjWgw' 
                                    onClose={() => this.setState({isOpen: false})} 
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
 
export default Banner;