import React from 'react';
import { Link } from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel3';
import '../../../node_modules/react-modal-video/scss/modal-video.scss';
import shapeImg from '../../assets/images/1.png';

import db from '../../db.json'

const options = {
    items: 1,
    loop: true,
    nav: true,
    dots: true,
    smartSpeed: 2000,
    margin: 20,
    autoplayHoverPause: true,
    autoplay: true,
    navText: [
        "<i class='flaticon-back'></i>",
        "<i class='flaticon-right'></i>"
    ],
}
 
class Testimonials extends React.Component {



    renderProfile = (profile) => {
        return (
            <div className="item" key={profile.title}>

                <div className="uk-grid uk-grid-match uk-grid-medium uk-child-width-1-2@m uk-child-width-1-1@s">

                    <div className="item">
                        <div className="feedback-img">
                            <center><img src={require(`../../assets/photos/${profile.image}`)} style={{flex: 'unset', width: 'unset'}} /></center>
                            <img src={shapeImg} className="shape-img" alt="Testimonials" style={{width: '100px', top: '10px'}} />
                        </div>
                    </div>

                    <div className="item">

                        <div className="uk-section-title section-title">
                            <span>What I do</span>
                            <h2 style={{margin: '20px 0px 20px 0px'}}>{profile.title}</h2>
                            <div className="bar"></div>
                        </div>

                        <div className="single-feedback">

                            {/* <i className="flaticon-quote"></i> */}
                            <p>{profile.description}</p>

                            {/*}
                            <div className="client">
                                <h3>Jason Statham</h3>
                                <span>Founder at Envato</span>
                            </div>
                            */}
                        </div>
                    </div>

                </div>

            </div>
        )
    }



    render(){
        return (
            <section id="clients" className="feedback-area  uk-section uk-feedback" style={{marginBottom: '50px'}}>
                <div className="uk-container">

                    <OwlCarousel 
                        className="feedback-slides owl-carousel owl-theme"
                        {...options}
                    >
                        {db.profiles.map(this.renderProfile)}

                    </OwlCarousel>
                </div>
            </section>
        );
    }
}
 
export default Testimonials;