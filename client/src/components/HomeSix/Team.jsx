import React from 'react';
import { Link } from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel3';
import teamOne from '../../assets/images/team1.jpg';
import teamTwo from '../../assets/images/team2.jpg';
import teamThree from '../../assets/images/team3.jpg';
import teamFour from '../../assets/images/team4.jpg';
import teamFive from '../../assets/images/team5.jpg';

import db from '../../db.json'

const options = {
    loop: false,
    nav: true,
    dots: true,
    smartSpeed: 2000,
    margin: 15,
    autoplayHoverPause: true,
    autoplay: false,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 2
        },
        786: {
            items: 2
        },
        1200: {
            items: 3
        }
    }
}
 
class Team extends React.Component {
    render(){
        return (
            <section id="team" className="team-area  uk-team uk-section">
                <div className="uk-container">
                    <div className="uk-section-title section-title">
                        {/* <span>Where I worked</span> */}
                        <h2>Companies</h2>
                        <div className="bar"></div>

                        {/* <Link to="#" className="uk-button uk-button-default">View All</Link> */}
                    </div>
                </div>

                <OwlCarousel 
                    className="team-slides owl-carousel owl-theme"
                    {...options}
                >
                    {db.companies.map(c =>
                        <div className="single-team">
                            {/*
                            <ul className="team-social">
                                <li><Link to="#"><i className="flaticon-logo"></i></Link></li>
                                <li><Link to="#"><i className="flaticon-twitter"></i></Link></li>
                                <li><Link to="#"><i className="flaticon-linkedin"></i></Link></li>
                                <li><Link to="#"><i className="flaticon-logo-1"></i></Link></li>
                            </ul>
                            */}
                            <ul className="team-social">
                                <div>{c.dates}</div>
                                <div>{c.details}</div>
                            </ul>

                            <img src={require(`../../assets/photos/${c.image}`)} alt="team" />

                            <div className="team-content">
                                <h3>{c.name}</h3>
                                <span>{c.title}</span>
                            </div>
                        </div>
                    )}

                </OwlCarousel>
            </section>
        );
    }
}
 
export default Team;