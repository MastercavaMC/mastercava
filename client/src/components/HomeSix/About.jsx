import React from 'react';
import lax from 'lax.js';
import signature from '../../assets/images/signature.png';
import about1 from '../../assets/photos/datacenter.jpg';
import about2 from '../../assets/photos/thevoid.jpg';
import one from '../../assets/images/1.png';
import LaxButton from '../Common/LaxButton';
 
class About extends React.Component {
    constructor(props) {
        super(props)
        lax.setup()
    
        document.addEventListener('scroll', function(x) {
            lax.update(window.scrollY)
        }, false)
    
        lax.update(window.scrollY)
    }
    render(){
        return (
            <section id="about" className="uk-about  about-area uk-section">
                <div className="uk-container">
                    <div className="uk-grid uk-grid-match uk-grid-medium uk-child-width-1-2@m uk-child-width-1-1@s">
                        <div className="item">
                            <div className="about-content">
                                <div className="uk-section-title section-title">
                                    {/* <span>About Me</span> */}
                                    <h2>About Me</h2>
                                    <div className="bar"></div>
                                </div>

                                <div className="about-text">
                                    <div className="icon">
                                        <i className="flaticon-quote"></i>
                                    </div>
                                    <h3>I do <b>cool</b> things.</h3>
                                    <p>
                                        I am a creative and highly passionate person that loves cutting-edge technology and innovation.<br />
                                        Currently working as a <b>Research and Development Manager</b> at <a href="http://www.apple.com" target="_blank" style={{display: 'inline'}}>Apple</a>,
                                        my peculiarity is to combine years of engineering experience with strong design and communication skills, and efficiently apply them to solve some of the most interesting research problems.
                                    </p>
                                    <p>
                                        My interests span widely across the field of human-computer interaction, touching topics such as Data Science and Visual Analytics, Augmented and Virtual Reality, Machine Learning and Computer Vision, Finance, and Healthcare.
                                        I like to arm myself with a broad set of skills, since I believe the best opportunities (and the greatest fun) arise from the intersection of multiple fields.
                                    </p>

                                    {/*<div className="signature">
                                        <img src={signature} alt="signature" />
                                    </div>*/}


                                </div>
                            </div>
                        </div>

                        <div className="item">
                            <div className="about-img" style={{marginTop: '50px'}}>
                                <img src={about1} className="about-img1" alt="about-img" style={{width: '700px'}} />
                                <img src={about2} className="about-img2" alt="about-img" style={{height: '300px', left: '-90px'}} />
                                <img src={one} style={{left: '10%'}} className="shape-img" alt="shape" />

                                <LaxButton />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
 
export default About;