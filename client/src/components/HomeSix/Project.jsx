import React from 'react';
import { Link } from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel3';
import db from '../../db.json';

const options = {
    loop: false,
    nav: true,
    dots: true,
    smartSpeed: 2000,
    margin: 30,
    autoplayHoverPause: true,
    autoplay: false,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 2
        },
        786: {
            items: 2
        },
        1200: {
            items: 4
        }
    },
    navText: [
        "<i class='flaticon-back' style=\"margin-left: 250px\"></i>",
        "<i class='flaticon-right' style=\"margin-right: 250px\"></i>"
    ],
}


const iconStyle = {
    textDecoration: 'none',
    // marginLeft: '10px',
    // marginRight: '10px',
    border: '1px solid',
    // width: '30px',
    // height: '30px',
    // lineHeight: '28px',
}
 
class Project extends React.Component {

    state = {
        selectedIndex: undefined
    }

    render(){

        const {selectedIndex} = this.state

        return (
            <section id="project" className="project-area  uk-dark uk-project uk-section">
                <div className="uk-container">
                    <div className="uk-section-title section-title">
                        {/* <span>Our Completed Projects</span> */}
                        <h2>Selected Publications</h2>
                        <div className="bar"></div>

                        <a target="_blank" to="https://scholar.google.com/citations?user=DqPB3ZgAAAAJ" className="uk-button uk-button-default">View on Google Scholar</a>
                    </div>
                </div>

                <OwlCarousel 
                    className="feedback-slides owl-carousel owl-theme"
                    {...options}
                >
                    {db.papers.map((p,i) =>
                        <div className="single-project">
                        {/* onMouseEnter={() => this.setState({selectedIndex: i})} */}s
                            <a className="project-img">
                                <img src={require(`../../assets/photos/${p.image}`)} alt="Project" />
                            </a>

                            <div className="project-content">
                                <h3>{p.title}</h3>
                                <div className="single-footer-widget">
                                    <ul className="social" style={{marginTop: '12px'}}>
                                        {p.paper && <li><a href={p.paper} target="_blank" style={iconStyle}> <i className="flaticon-link"></i></a></li>}
                                        {p.video && <li><a href={p.video} target="_blank" style={iconStyle}> <i className="flaticon-multimedia"></i></a></li>}
                                    </ul>
                                    <ul style={{marginTop: '12px'}}>
                                        <li>
                                            {p.publisher}
                                        </li>
                                    </ul>
                                </div>
                                {/*selectedIndex === i ?
                                    <div className="single-footer-widget">
                                        <ul className="social">
                                            {p.paper && <li><a href={p.paper} target="_blank" style={iconStyle}> <i className="flaticon-link"></i></a></li>}
                                            {p.video && <li><a href={p.video} target="_blank" style={iconStyle}> <i className="flaticon-multimedia"></i></a></li>}
                                        </ul>
                                    </div>
                                    :
                                    <ul>
                                        <li>
                                            {p.publisher}
                                        </li>
                                    </ul>
                                */}
                            </div>
                        </div>
                    )}

                </OwlCarousel>

                <div className="uk-container" style={{fontSize: '1.2em', marginTop: '100px'}}>
                    You can see the full list of <strong style={{color: 'white'}}>publications</strong> and <strong style={{color: 'white'}}>patents</strong> on my <a style={{display: 'inline'}} href="https://scholar.google.com/citations?user=DqPB3ZgAAAAJ" target="_blank">Google Scholar profile</a> :)
                </div>
            </section>
        );
    }
}
 
export default Project;