import React from 'react';
import OwlCarousel from 'react-owl-carousel3';

import db from '../../db.json'

const options = {
    loop: false,
    nav: true,
    dots: true,
    smartSpeed: 1000,
    margin: 30,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 2
        },
        786: {
            items: 2
        },
        1200: {
            items: 3
        }
    },
    navText: [
        "<i class='flaticon-back' style=\"margin-left: 250px\"></i>",
        "<i class='flaticon-right' style=\"margin-right: 250px\"></i>"
    ],
}
 
class LatestNews extends React.Component {
    render(){
        return (
            <section id="blog" className="blog-area  uk-blog uk-section" style={{marginBottom: '50px'}}>
                <div className="uk-container">
                    <div className="uk-section-title section-title">
                        {/* <span>Conferences and public talks</span> */}
                        <h2>Conferences and public talks</h2>
                        <div className="bar"></div>

                        {/*<Link to="#" className="uk-button uk-button-default">View All</Link>*/}
                    </div>

                    <OwlCarousel 
                        className="feedback-slides owl-carousel owl-theme"
                        {...options}
                    >

                        {db.events.map(e => 
                            <div key={e.title} className="single-blog-post">
                                <div className="blog-post-image">
                                    <a target="_blank">
                                        <img src={require(`../../assets/photos/${e.image}`)} alt="news" />
                                    </a>
                                </div>

                                <div className="blog-post-content">
                                    <span className="date">{e.date}</span>
                                    <h3><a target="_blank">{e.title}</a></h3>
                                    <div>{e.description}</div>
                                    {/*<Link to="#" className="read-more">Read More</Link>*/}
                                </div>
                            </div>
                        )}

                    </OwlCarousel>
                </div>
            </section>
        );
    }
}
 
export default LatestNews;