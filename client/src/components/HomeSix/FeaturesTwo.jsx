import React from 'react';
import db from '../../db.json'

 
class FeaturesTwo extends React.Component {
    render(){
        return (
            <section className="uk-features uk-dark features-area" style={{zIndex: 0}}>
                <div className="uk-container">
                    <div className="uk-grid uk-grid-match uk-grid-medium uk-child-width-1-3@m">

                        {db.information.features.map((f,i) =>

                            <div className="uk-item">
                                <div className={`single-features-box${(i+1)%2 === 0 ? ' active' : ''}`}>
                                    <div className="icon">
                                        <i className={f.icon}></i>
                                    </div>
                                    <h3>{f.title}</h3>
                                    <div className="bar"></div>
                                    <p>{/*f.description*/}</p>

                                    <div className="dot-img">
                                        <img src={require("../../assets/images/dot.png")} alt="dot" className="color-dot" />
                                        <img src={require("../../assets/images/white-dot.png")} alt="dot" className="white-dot" />
                                    </div>

                                    <div className="animation-img">
                                        <img src={require("../../assets/images/shape1.svg")} alt="shape" />
                                        <img src={require("../../assets/images/shape2.svg")} alt="shape" />
                                        <img src={require("../../assets/images/shape3.svg")} alt="shape" />
                                        <img src={require("../../assets/images/shape1.svg")} alt="shape" />
                                        <img src={require("../../assets/images/shape2.svg")} alt="shape" />
                                        <img src={require("../../assets/images/shape3.svg")} alt="shape" />
                                        <img src={require("../../assets/images/shape1.svg")} alt="shape" />
                                        <img src={require("../../assets/images/shape3.svg")} alt="shape" />
                                    </div>
                                </div>
                            </div>

                        )}
                        
                    </div>
                </div>
            </section>
        );
    }
}
 
export default FeaturesTwo;