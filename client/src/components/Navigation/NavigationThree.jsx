import '../../../node_modules/uikit/dist/css/uikit.min.css';
import '../../assets/css/style.scss';
import '../../assets/css/responsive.scss';
import '../../assets/css/flaticon.css';
import '../../assets/css/animate.min.css';
import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import logo from '../../assets/images/logo2.png';
 
class Navigation extends React.Component {

    render(){
        return (
            <React.Fragment>
                {/* Start Mobile Navbar */}
                <div id="offcanvas-flip" className="mobile-navbar uk-mobile-navbar" uk-offcanvas="flip: true; overlay: true">
                    <div className="uk-offcanvas-bar">
                
                        <button className="uk-offcanvas-close" type="button" uk-close="true"></button>
                
                        <nav className="uk-navbar-container" data-uk-scrollspy-nav="offset: 0; closest: li; scroll: true">
                            <ul className="uk-navbar-nav">
                                <li className="uk-active">
                                    <AnchorLink offset={() => 100} href="#home">Home</AnchorLink>
                                </li>
                                <li>
                                    <AnchorLink offset={() => 100} href="#about">About</AnchorLink>
                                </li>
                                <li>
                                    <AnchorLink offset={() => 100} href="#services">Skills</AnchorLink>
                                </li>
                                <li>
                                    <AnchorLink offset={() => 100} href="#team">Companies</AnchorLink>
                                </li>
                                <li>
                                    <AnchorLink offset={() => 100} href="#project">Publications</AnchorLink>
                                </li>
                                <li>
                                    <AnchorLink offset={() => 100} href="#blog">Conferences</AnchorLink>
                                </li>
                                <li>
                                    <a target="_blank" href="/resume.pdf">Resume</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                {/* End Mobile Navbar */}

                {/* Start Navbar Area */}
                <header className="header-area uk-dark" data-uk-sticky="animation: uk-animation-slide-top;">
                    <div className="uk-container">
                        <div className="uk-navbar">
                            <div className="logo uk-navbar-left">
                                <a 
                                    href="/"
                                >
                                    {<img src={require("../../assets/photos/signature.png")} alt="logo" style={{maxHeight: '60px'}} />}
                                </a>
                            </div>

                            <div className="uk-navbar-toggle" id="navbar-toggle" uk-toggle="target: #offcanvas-flip">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>

                            <div className="navbar uk-navbar-right">
                                <nav className="uk-navbar-container" data-uk-scrollspy-nav="offset: 0; closest: li; scroll: true">
                                    <ul className="uk-navbar-nav">
                                        <li className="uk-active">
                                            <AnchorLink
                                                href="#home"
                                                offset={() => 100}
                                            >
                                                Home
                                            </AnchorLink>
                                        </li>
                                        <li>
                                            <AnchorLink 
                                                offset={() => 100} 
                                                href="#about"
                                            >
                                                About
                                            </AnchorLink>
                                        </li>
                                        <li>
                                            <AnchorLink 
                                                href="#services"
                                                offset={() => 100}
                                            >
                                                Skills
                                            </AnchorLink>
                                        </li>
                                        <li>
                                            <AnchorLink 
                                            href="#team"
                                            offset={() => 100}>Companies</AnchorLink>
                                        </li>
                                        <li>
                                            <AnchorLink 
                                                href="#project"
                                                offset={() => 100}
                                            >
                                                Publications
                                            </AnchorLink>
                                        </li>
                                        <li>
                                            <AnchorLink 
                                            href="#blog"
                                            offset={() => 100}>Conferences</AnchorLink>
                                        </li>
                                        <li>
                                            <a target="_blank" href="/resume.pdf">Resume</a>
                                        </li>
                                    </ul>
                                </nav>

                            </div>
                        </div>
                    </div>
                </header>
                {/* End Navbar Area */}
            </React.Fragment>
        );
    }
}
 
export default Navigation;