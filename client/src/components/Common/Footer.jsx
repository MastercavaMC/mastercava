import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo2.png';
import footerShapeOne from '../../assets/images/footer-shape1.png';
import footerShapeTwo from '../../assets/images/footer-shape2.png';
import GoTop from './GoTop';
import mapImg from '../../assets/images/map.png';
// import ironman3 from '../../assets/photos/ironman3.png';
import chibiHead from '../../assets/photos/chibi_head.png';
// import ironman7 from '../../assets/photos/ironman7.png';
 
class Footer extends React.Component {
    render(){
        return (
            <footer className="footer-area uk-dark uk-footer" style={{paddingTop: '50px'}}>
                <div className="uk-container">
                    <div className="uk-grid uk-grid-match uk-grid-medium uk-child-width-1-4@m uk-child-width-1-2@s">
                        {/*
                        <div className="item">
                            <div className="single-footer-widget">
                                <div className="logo">
                                    <Link to="index.html">
                                        <img src={logo} alt="logo" />
                                    </Link>
                                </div>

                                <p>Lorem ipsum dolor consectetur adipiscing elit, eiusmod tempor ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>

                        <div className="item">
                            <div className="single-footer-widget">
                                <h3>New York</h3>
                                <div className="bar"></div>

                                <div className="location">
                                    <p>198 Collective Street <br /> Manhattan <br /> Kingston <br /> United State</p>
                                </div>
                            </div>
                        </div>

                        <div className="item">
                            <div className="single-footer-widget">
                                <h3>London</h3>
                                <div className="bar"></div>

                                <div className="location">
                                    <p>352/71 Second Street <br /> King Street <br /> Kingston <br /> United Kingdom</p>
                                </div>
                            </div>
                        </div>
                        */}

                        
                        <div className="item uk-width-1-6">
                            <img src={chibiHead} style={{marginTop: '-30px', objectFit: 'contain'}} />
                        </div>
                    

                        {/*
                        <div className="item uk-width-1-2">
                            
                            <p><small>
                                My main research revolves around the concept of Intelligence Augmentation, i.e. using information technology to supplement and support human thinking. 
                                In particular, my interests focus on exploring how cutting-edge technologies can be combined to create hybrid, interactive systems able to augment human capabilities in collaborative settings. The final aim is to improve the way people communicate and interact with each other and with machines, building systems that will enhance how people currently perform their daily tasks and handle digital content.
                            </small></p>
                            
                        </div>
                        */}

                        <div className="item uk-width-1-4">
                            <div className="single-footer-widget">
                                <ul className="social" style={{float: 'right', marginTop: '-8px'}}>
                                    <li><a href="https://www.linkedin.com/in/mastercava/" target="_blank"><i className="flaticon-linkedin"></i></a></li>
                                </ul>
                                <h3>Find me on LinkedIn</h3>
                                <div className="bar"></div>
                                {/*
                                <ul className="contact-info">
                                    <li><Link to="#">info@gunter.com</Link></li>
                                    <li><Link to="#">fax@gunter.com</Link></li>
                                    <li><Link to="#">+44 478 541 7452</Link></li>
                                </ul>
                                */}
                                
                            </div>
                        </div>



                        {/*
                        <div className="item">
                            <div className="map-img">
                                <img src={mapImg} alt="map" />

                                <div className="location uk-location1">
                                    <Link to="#">
                                        <div className="location-info">
                                            <h5>New York</h5>
                                            <span>198 Collective Street</span>
                                        </div>
                                    </Link>
                                </div>

                                <div className="location uk-location2">
                                    <Link to="#">
                                        <div className="location-info">
                                            <h5>London</h5>
                                            <span>357/71 Collective Street</span>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        */}



                    </div>

                    <div className="copyright-area" style={{marginTop: '0px'}}>
                        <div className="uk-grid uk-grid-match uk-grid-medium uk-child-width-1-2@m uk-child-width-1-2@s">
                            <div className="item">
                                <p>© Mastercava. All Rights Reserved, 2019</p>
                            </div>

                            {/*
                            <div className="item">
                                <ul>
                                    <li><Link to="#">Terms & Conditions</Link></li>
                                    <li><Link to="#">Privacy Policy</Link></li>
                                </ul>
                            </div>
                            */}
                        </div>
                        <GoTop scrollStepInPx="50" delayInMs="16.66" />
                    </div>
                </div>

                <div className="br-line"></div>
                <div className="footer-shape1">
                    <img src={footerShapeOne} alt="shape" />
                </div>
                <div className="footer-shape2">
                    <img src={footerShapeTwo} alt="shape" />
                </div>
            </footer>
        );
    }
}
 
export default Footer;