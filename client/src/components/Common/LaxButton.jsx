import React from 'react'
import ReactDOM from 'react-dom';
import lax from 'lax.js';
import { Link } from 'react-router-dom';
 
class LaxButton extends React.Component {
    componentDidMount() {
        this.el = ReactDOM.findDOMNode(this)
        lax.addElement(this.el)
    }
    
    componentWillUnmount() {
        lax.removeElement(this.el)
    }

    render(){
        return (
            <a href="/resume.pdf" target="_blank" className="uk-button uk-button-default lax" data-lax-preset="driftLeft">Download resume <i className="flaticon-right"></i></a>
        );
    }
}
 
export default LaxButton;